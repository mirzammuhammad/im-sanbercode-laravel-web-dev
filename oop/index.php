<?php
require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$shaun = new Animal ("Sheep");
echo "Nama hewan : " . $shaun -> name . "<br>";
echo "Jumlah kaki : " . $shaun -> legs . "<br>";
echo "Berdarah dingin : " . $shaun -> clood_blood . "<br>";

echo "<br>";

$sungokong = new Ape ("Kera Sakti");
echo "Nama hewan : " . $sungokong -> name . "<br>";
echo "Jumlah kaki : " . $sungokong -> legs . "<br>";
echo "Berdarah dingin : " . $sungokong -> clood_blood . "<br>";
echo $sungokong->menyeru("Auooooooo!!");

echo "<br><br>";

$kodok = new Frog ("Budok");
echo "Nama hewan : " . $kodok -> name . "<br>";
echo "Jumlah kaki : " . $kodok -> legs . "<br>";
echo "Berdarah dingin : " . $kodok -> clood_blood . "<br>";
echo $kodok->melompat("Hop Hop");

?>

