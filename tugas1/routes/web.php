<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

Route::get('/', [HomeController::class, 'home']);

Route::get('/pendaftaran', [AuthController::class, 'daftar']);

Route::post('/kirim', [AuthController::class, 'welkom']);