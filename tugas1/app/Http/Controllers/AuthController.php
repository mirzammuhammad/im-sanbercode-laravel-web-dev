<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view ('page.register');
    }

    public function welkom(Request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request ['lname'];
        $jenisKelamin = $request ['gender'];
        $kebangsaan = $request ['nationality'];
        $bahasa = $request ['language'];
        $biodata = $request ['bio'];

        return view ('page.welkom', ['namaDepan'=> $namaDepan, 'namaBelakang' => $namaBelakang, 'jenisKelamin' => $jenisKelamin, 'kebangsaan' => $kebangsaan, 'bahasa' => $bahasa, 'biodata' => $biodata]);
    }
}
